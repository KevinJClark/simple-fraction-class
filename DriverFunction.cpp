//After compiling the Fraction class, compile the main function with: g++ Fraction.o DriverFunction.cpp
//Run with ./a.out

#include "Fraction.h"
using namespace std;

int main()
{
	//Create 5 instances of "Fraction" -- Each has its own data
	Fraction F1(5, 10);		// -- 1/2
	Fraction F2(10, 20);	// -- 1/2
	Fraction F3;			// -- 0/1
	Fraction F4;			// -- 0/1
	Fraction F5;			// -- 0/1
	
	if(F1.IsEqual(F2))
	{
		cout << "F1 and F2 are equal" << endl;
	}
	else
	{
		cout << "F1 and F2 are not equal" << endl;
	}
        
	F3 = F1.Add(F2);
	F3.Write(cout); //Should come out to be 1/1
        
	cout << "Enter a fraction: ";
	F4.Read(cin);
	cout << endl << "F4 is now: ";
	F4.Write(cout);
        
	F5 = F4.Add(F2);
	cout << "F4 + F2 is: ";
	F5.Write(cout);
	cout << endl;

	if(F5.IsEqual(F3))
	{ //Only way to get this is if the number entered is 1/2 (or an equivalent like 2/4)
		cout << "F5 and F3 are equal" << endl;
	}
	else
	{
		cout << "F5 and F3 are not equal" << endl;
	}
	
	return 0; //Program completed without error
}
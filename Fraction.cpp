//Fraction class implementation file
//Compile with g++ -c Fraction.cpp

#include "Fraction.h"

//Reduced and simplifies fractions into improper fractions (like 9/7 or 12/1)
//This is a private function and can only be called from within the class itself
void Fraction::simplify()
{
	int Temp, GCD1 = Denominator, GCD2 = Numerator;
	while (GCD1 != 0)
	{
		Temp = GCD1;
		GCD1 = GCD2 % GCD1;
		GCD2 = Temp; //GCD 2 eventually will be the GCD
	}
	Numerator = Numerator / GCD2;
	Denominator = Denominator / GCD2;
}

//Method that takes a fraction as an argument and adds them together.
//Original fraction remains unchanged. The resultant fraction, "Sum" is returned
Fraction Fraction::Add (const Fraction AddFraction) const
{
	Fraction Sum;
	Sum.Denominator = Denominator * AddFraction.Denominator;
	Sum.Numerator = (Numerator * AddFraction.Denominator) + (AddFraction.Numerator * Denominator);
	Sum.simplify();
	return Sum;
}

//Method that takes another fraction as an argument. Returns true if they are equivalent (ex: 1/2 and 4/8)
bool Fraction::IsEqual(const Fraction EqFraction) const
{
	if (Numerator * EqFraction.Denominator == EqFraction.Numerator * Denominator)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//Writes to an output stream, like std::cout or a file
void  Fraction::Write(ostream& Output) const
{
	Output << Numerator << "/" << Denominator << endl;
}

//Reads from an input stream like std::cin or a file
bool Fraction::Read(istream& input)
{
	char Slash;
	if (input >> Numerator >> Slash >> Denominator) //Set class values according to input
	{
		simplify(); //If everything worked correctly, simplify the fraction
	}
	return input;
}
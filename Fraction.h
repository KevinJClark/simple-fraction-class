#include <iostream>
#ifndef FRACTION //If FRACTION is not defined...
#define FRACTION
using namespace std;

class Fraction
{
	//Private -- private methods and data can ONLY be accessed from inside the class
	private:
		int Numerator, Denominator;
		void simplify();

	//Public -- public methods and data can be accessed from outside the class
	public:
	
		//Constructor function
		//If numerator and denominator values are not given, defaults to 0/1
		Fraction(int n = 0, int d = 1)
		{
			Numerator = n;
			Denominator = d;
			simplify();
		}
		
		//Return the numerator
		int GetNumerator() const
		{
			return Numerator;
		}
		
		//Return the denominator
		int GetDenominator() const
		{
			return Denominator;
		}
        
		//Pretty print the fraction
		//Note: Cannot be printed to a file since std::cout is hard coded as the output
		void Print() const
		{
			cout << "Numerator: " << Numerator << "  Denominator: " << Denominator << endl;
		}
        
		//Function prototypes -- fully defined in the Fraction.cpp files
		Fraction Add(const Fraction AddFraction) const;
		bool IsEqual(const Fraction EqFraction) const;
		bool Read(istream&);
		void Write(ostream&) const;
};
#endif